/**
 **
 ** Calcul des heures profs pondérées : voir fonction 'maj_stats'
 **
 **/

/*
 * Un niveau contient plusieurs sections qui contient plusieurs classes.
 * Exemple: Niveau: 2nd  ; Section: 2GT, SNT  ;  Classe: 2GT1, 2GT2,...
 */


//
//
// Classes générales
//
//

/*
 * classe gerant le drag n'drop
 */
class TireEtLache {

    // les dropzones
    
    /*
     * Méthode principale pour les dropzones
     */
    static dropzone(element, drop_it_here_function, modify_apparence=true) {
        element.addEventListener('dragover', function(evt) { TireEtLache.over_dropzone(evt, element, modify_apparence); });
        element.addEventListener('dragleave', function(evt) { TireEtLache.leave_dropzone(evt, element, modify_apparence); });
        element.addEventListener('drop', function(evt) { TireEtLache.drop_dropzone(evt, element, drop_it_here_function, modify_apparence); });
        element.classList.add("droppable_zone");
    }
    
    static over_dropzone(evt, elmt, modify_apparence) {
        if (modify_apparence) {
            elmt.classList.remove("droppable_zone");
            elmt.classList.add("dropped_zone");
        }
        evt.dataTransfer.dropEffect = "move";
        evt.preventDefault();
    }
    
    static leave_dropzone(evt, elmt, modify_apparence) {
        if (modify_apparence) {
            elmt.classList.remove("dropped_zone");
            elmt.classList.add("droppable_zone");
        }
    }
    
    static drop_dropzone(evt, elmt, fct, modify_apparence) {
        evt.preventDefault();
        if (modify_apparence) {
            elmt.classList.remove("dropped_zone");
            elmt.classList.add("droppable_zone");
        }
        let target = document.getElementById(evt.dataTransfer.getData('text/plain'));
        fct(evt, target, elmt, null);
    }

    // les droppables

    /*
     * Méthode principale pour les droppables
     */
    static droppable(element, drop_it_here_function, type_list) {
        // type_list = "horiz" ou "verti"
        element.addEventListener('dragstart', function(evt) { TireEtLache.start_drag(evt, element); });
        element.addEventListener('dragend', function(evt) { TireEtLache.end_drag(evt, element); });
        element.addEventListener('dragenter', function(evt) { TireEtLache.enter_drag(evt, element); });
        element.addEventListener('dragleave', function(evt) { TireEtLache.leave_drag(evt, element); });
        element.addEventListener('drop', function(evt) { TireEtLache.drop_drag(evt, element, drop_it_here_function); }, true);
        element.classList.add("draggable_obj");
        if (type_list == "horiz") element.classList.add("horizontal_list");
        if (type_list == "verti") element.classList.add("vertical_list");
        element.draggable = true;
    }
    
    static start_drag(evt, elmt) {
        evt.dataTransfer.setData('text/plain', elmt.id);
        evt.dataTransfer.effectAllowed = 'move';
        elmt.classList.remove("draggable_obj");
        elmt.classList.add("dragged_obj");
    }
    
    static end_drag(evt, elmt) {
        elmt.classList.remove("dragged_obj");
        elmt.classList.add("draggable_obj");
    }

    static enter_drag(evt, elmt) {
        elmt.classList.add("draggable_dragover");
    }

    static leave_drag(evt, elmt) {
        elmt.classList.remove("draggable_dragover");
    }
    
    static drop_drag(evt, elmt, fct) {
        evt.preventDefault()
        evt.stopPropagation();
        let dropzone = elmt;
        while (!(dropzone.classList.contains("dropped_zone") || dropzone.classList.contains("droppable_zone"))) {
            dropzone = dropzone.parentElement;
        }
        dropzone.classList.remove("dropped_zone");
        dropzone.classList.add("droppable_zone");
        elmt.classList.remove("draggable_dragover");
        let target = document.getElementById(evt.dataTransfer.getData('text/plain'));
        fct(evt, target, dropzone, elmt);
    }
    
}




/*
 * Classe générale gérant un onglet et un bouton
 */
class GeneralTab {
    constructor(tabid, buttonid=null, subtabs=null, button_classes=null) {
        this.tab = document.getElementById(tabid);
        this.button = (buttonid !== null ? document.getElementById(buttonid) : null);
        if (subtabs === null) { subtabs = []; }
        this.sub_tabs = subtabs;
        if (button_classes === null) { button_classes = {"active":"", "passive":""}; }
        this.but_class = button_classes;

        if (this.is_shown()) {
            if (this.button != null) this.button.disabled = true;
            if (this.but_class.active.length > 0) { this.button.classList.add(this.but_class.active); }
            if (this.but_class.passive.length > 0) { this.button.classList.remove(this.but_class.passive); }
        } else {
            if (this.button != null) this.button.disabled = false;
            if (this.but_class.active.length > 0) { this.button.classList.remove(this.but_class.active); }
            if (this.but_class.passive.length > 0) { this.button.classList.add(this.but_class.passive); }
        }
    }

    is_shown() {
        return !this.tab.classList.contains("passive_tab");
    }

    
    show_me(selected, saveit=true) {
        let old_sel = this.is_shown();
        if (old_sel == selected) { return; }
        this.tab.classList.toggle("passive_tab");
        if (this.button != null) this.button.disabled = selected;
        if (this.but_class.active.length > 0) { this.button.classList.toggle(this.but_class.active); }
        if (this.but_class.passive.length > 0) { this.button.classList.toggle(this.but_class.passive); }
        if (selected) {
            this.init_tab();
        } else {
            for (let k=0; k<this.sub_tabs.length; k++) { this.sub_tabs[k].show_me(false, saveit); }
            if (saveit) { this.save_me(); }
        }
    }

    init_tab() {
    }

    save_me() {
    }
}



/*
 * Classe générale gérant plusieurs onglets
 */
class MultiTab {
    constructor(tabs) {
        this.tabs = tabs;
    }

    select_tab(n) {
        for (let k=0; k<this.tabs.length; k++) {
            if (n == k) continue;
            this.tabs[k].show_me(false);
        }
        this.tabs[n].show_me(true);
    }

    recharge() {
        for (let k=0; k<this.tabs.length; k++) {
            if (this.tabs[k].is_shown()) { this.tabs[k].init_tab(); }
        }
    }
}



/*
 * Classe gérant le undo/redo
 */
class UndoRedo {
    constructor(id_undo_but, id_redo_but) {
        this.pile = [];
        this.index = -1;
        this.unbut = document.getElementById(id_undo_but);
        this.rebut = document.getElementById(id_redo_but);
    }

    empile(newdata) {
        let stringdata = JSON.stringify(newdata);
        let oldstringdata = this.index >= 0 ? this.pile[this.index] : "";
        if (stringdata != oldstringdata) {
            while (this.index + 1 < this.pile.length) { this.pile.pop(); }
            this.pile.push(stringdata);
            this.index++;
            this.unbut.disabled = false;
            this.rebut.disabled = true;
            console.log("empile. taille de la pile: " + this.pile.length + "\tindex courant:" + this.index);
            return true;
        }
        return false;
    }

    undo(currentdata) {
        let currentstringdata = JSON.stringify(currentdata);
        let indexstringdata = this.index >= 0 ? this.pile[this.index] : "";
        if (currentstringdata == indexstringdata) {
            this.index--;
            return this.undo(currentdata);
        }
        let nextstringdata = this.index+1 < this.pile.length ? this.pile[this.index+1] : "";
        if (currentstringdata != nextstringdata) {
            this.empile(currentdata);
            this.index--;
        }
        let data = JSON.parse(indexstringdata);
        
        this.unbut.disabled = (this.index == 0);
        this.rebut.disabled = false;
        console.log("undo. taille de la pile: " + this.pile.length + "\tindex courant:" + this.index);
        return data;
    }

    redo() {
        let data = null;
        if (this.index+1 < this.pile.length) {
            this.index++;
            data = JSON.parse(this.pile[this.index]);
        }
        this.unbut.disabled = false;
        this.rebut.disabled = (this.index+1 == this.pile.length);
        console.log("redo. taille de la pile: " + this.pile.length + "\tindex courant:" + this.index);
        return data;
    }
}


//
//
// Classes gérant les données
//
//


/*
 * Classe gérant un niveau (2nd, 1ere, tal, bts, prepa...)
 */
class Niveau {
    static max_id = 1;
    static object_list = {};

    static register(obj) {
        Niveau.object_list[obj.id] = obj;
    }

    static unregister(id) {
        for (let k=0; k<Niveau.object_list[id].sections.length; k++) { Section.unregister(Niveau.object_list[id].sections[k].id); }
        delete Niveau.object_list[id];
    }

    static get_by_id(id) {
        if (id in Niveau.object_list) return Niveau.object_list[id];
        return null;
    }
    
    constructor(data={"id":-1, "nom":"", "poids":"2nd", "sections":[]}) {
        this.id = data.id;
        this.nom = data.nom;
        this.poids = data.poids;
        this.sections = [];
        for (let k=0; k<data.sections.length; k++) {
            this.sections.push(new Section(this, data.sections[k]));
        }
        if (this.id < 0) {
            this.id = Niveau.max_id;
            Niveau.max_id++;
        } else {
            if (Niveau.max_id <= this.id) {
                Niveau.max_id = this.id + 1;
            }
        }
        Niveau.register(this);
    }

    /*
     * transforme un objet Niveau en json.
     */
    jsonify() {
        let tab = { "id": this.id,
                    "nom": this.nom,
                    "poids": this.poids,
                    "sections": []};
        for (let k=0; k<this.sections.length; k++) {
            tab.sections.push(this.sections[k].jsonify());
        }
        return tab;
    }

    add_section(section) {
        this.sections.push(section);
    }

    remove_section(section) {
        repartition.empile(); /* Oh que c'est moche ! */
        let ind = -1;
        for (let k=0; k<this.sections.length; k++) {
            if (this.sections[k] == section) {
                ind = k;
                break;
            }
        }
        let htmlniv = document.getElementById("classe_liste_classes_" + this.id);
        if (htmlniv != null) {
            htmlniv.removeChild(htmlniv.childNodes[ind]);
        }
        this.sections.splice(ind,1);
        Section.unregister(section.id);
    }

    
    /*
     * Retourne l'objet HTML représentant le Niveau pour l'onglet des classes
     */
    html_element_for_classes_tab() {
        let lefield = document.createElement("FIELDSET");
        lefield.id = "classe_niveau_" + this.id;
        let inner = "Nom du niveau: <input id=\"classe_niveau_" + this.id + "_nom\" type=\"text\" value=\"" + this.nom + "\">";
        inner += "<span title=\"Permet de déterminer la pondération\"><span class=\"a_droite\">Type de niveau :</span>";
        inner += "<select id=\"classe_poids_" + this.id + "\" name=\"classe_poids_" + this.id + "\">";
        let opt = "";
        if (this.poids == "2nd") { opt = " selected"; } else { opt = ""; }
        inner += "<option value='2nd'" + opt + ">Seconde</option>";
        if (this.poids == "tal") { opt = " selected"; } else { opt = ""; }
        inner += "<option value='tal'" + opt + ">Cycle terminal</option>";
        if (this.poids == "bts") { opt = " selected"; } else { opt = ""; }
        inner += "<option value='bts'" + opt + ">Supérieur BTS</option>";
        if (this.poids == "cpge") { opt = " selected"; } else { opt = ""; }
        inner += "<option value='cpge'" + opt + ">Supérieur CPGE</option>";
        inner += "</select></span>";
        inner += "<button type=\"button\" onclick='repartition.classes_tab.remove_niveau(" + this.id + ")' class=\"a_droite\">Supprimer ce niveau</button>";
        inner += "<table><thead>";
        inner += "<tr><th><button type=\"button\" onclick='repartition.classes_tab.add_section(" + this.id + ")' title='Ajouter une classe'><img src='images/classe-add.png' alt='Ajouter classe' width='18'></button></th><th>Nom</th><th title=\"Nombres d'heures devant élèves\">nb</br>heures</th><th>nb</br>classes</th>";
        inner += "<th title=\"Pour rassembler les classes identiques qui n'ont pas le même volume horaire\">groupe</th>";
        inner += "<td></td></tr>";
        inner += "</thead><tbody id=\"classe_liste_classes_" + this.id + "\">";
        inner += "</tbody></table>";
        lefield.innerHTML = inner;
        return lefield;
    }

    get_dropzone_for_classes_tab() {
        return document.getElementById("classe_liste_classes_" + this.id);
    }
    
    /*
     * Retourne l'objet HTML représentant le nom du Niveau pour l'onglet des services
     */
    html_name_element_for_services_tab() {
        let element = document.createElement("TH");
        element.innerHTML = this.nom;
        return element;
    }

    /*
     * Retourne l'objet HTML représentant le conteneur du Niveau pour l'onglet des services
     */
    html_container_element_for_services_tab() {
        let element = document.createElement("TD");
        element.id = "service_niveau_" + this.id;
        return element;
    }

    /*
     *
     */
    fill_services_tab(prof_containers, default_container, drop_callback) {
        for (let s=0; s<this.sections.length; s++) {
            this.sections[s].fill_services_tab(prof_containers, default_container, drop_callback);
        }
    }
    

    get_displayed_name() {
        return document.getElementById("classe_niveau_" + this.id + "_nom").value;
    }

    get_displayed_type() {
        return document.getElementById("classe_poids_" + this.id).value;
    }
    
    /*
     * Sauve les données depuis l'onglet des classes
     */
    save_me() {
        repartition.empile();
        for (let s=0; s<this.sections.length; s++) {
            this.sections[s].save_me();
        }
        let dropz = this.get_dropzone_for_classes_tab();
        let newliste = [];
        for (let s=0; s<dropz.childElementCount; s++) {
            newliste.push(Section.get_from_node_in_classes_tab(dropz.children[s]));
        }
        this.nom = this.get_displayed_name();
        this.poids = this.get_displayed_type();
        this.sections = newliste;
    }
    
}


/*
 * Classe gérant une section dans tous les onglets
 */
class Section {
    static max_id = 1;
    static object_list = {};

    static register(obj) {
        Section.object_list[obj.id] = obj;
    }

    static unregister(id) {
        delete Section.object_list[id];
    }

    static get_by_id(id) {
        if (id in Section.object_list) return Section.object_list[id];
        return null;
    }
    
    constructor(niveau, data={"id":-1, "nom":"", "heures":1, "nb":1, "groupe":-1, "prof":[]}) {
        this.niveau = niveau;
        this.id = data.id;
        this.nom = data.nom;
        this.heures = data.heures;
        this.nb = data.nb;
        this.groupe = data.groupe;
        this.classes = [];
        for (let k=0; k<data.prof.length; k++) {
            this.classes.push(new Classe(this, data.prof[k],k));
        }
        for (let k=data.prof.length; k<this.nb; k++) {
            this.classes.push(new Classe(this, -1,k));
        }
        if (this.id < 0) {
            this.id = Section.max_id;
            this.groupe = this.id
            Section.max_id++;
        } else {
            if (Section.max_id <= this.id) {
                Section.max_id = this.id + 1;
            }
        }
        Section.register(this);
    }

    /*
     * transforme un objet Section en json.
     */
    jsonify() {
        let data = {"id":this.id, "nom":this.nom, "heures":this.heures, "nb":this.nb, "groupe":this.groupe, "prof":[]};
        for (let c=0; c<this.classes.length; c++) {
            if (this.classes[c].prof_id != -1)
                data.prof.push(this.classes[c].prof_id);
        }
        return data;
    }

    static focus_lost_in_classes_tab(section_id) {
        Section.get_by_id(section_id).save_me();
    }
    
    /*
     * Retourne le noeud HTML de la section pour l'onglet des classes
     */
    html_element_for_classes_tab() {
        let classe_ns = "classe_classe_" + this.id;
        let element = document.createElement("TR");
        element.id = classe_ns + "_init";
        element.classList.add("les_classes");
        let inner = "<td><img src='images/drag-reorder.png' alt='Réordonner' width='16' id='" + classe_ns + "_drag'></td>";
        inner += "<td><input id=\"" + classe_ns + "_nom\" type=\"text\" value=\""+ this.nom + "\" oninput='repartition.classes_tab.maj_groupes(" + this.niveau.id + ")' onfocusout='Section.focus_lost_in_classes_tab(" + this.id + ")'></td>";
        inner += "<td><input class=\"nombre\" id=\"" + classe_ns + "_heures\" title=\"Nombres d'heures devant élèves\" type=\"number\" step=\"any\" min=0 value=\""+ this.heures + "\"  onfocusout='Section.focus_lost_in_classes_tab(" + this.id + ")'></td>";
        inner += "<td><input class=\"nombre\" id=\"" + classe_ns + "_nb\" type=\"number\" min=0 value=\""+ this.nb + "\" onfocusout='Section.focus_lost_in_classes_tab(" + this.id + ")'></td>";
        inner += "<td><select id=\"" + classe_ns + "_groupe\" title=\"Pour rassembler les classes identiques qui n'ont pas le même volume horaire\" onchange='repartition.classes_tab.maj_groupes(" + this.niveau.id + ")' onfocusout='Section.focus_lost_in_classes_tab(" + this.id + ")'></select></td>";
        inner += "<td><button type=\"button\" onclick='repartition.classes_tab.remove_section(" + this.id + ")' title='Supprimer cette classe'><img src='images/classe-remove.png' alt='Supprimer classe' width='12'></button></td>";
        element.innerHTML = inner;
        return element;
    }

    get_html_draggable_element_in_classes_tab() {
        return document.getElementById("classe_classe_" + this.id + "_drag")
    }

    static get_html_node_from_html_drag_id(html_id) {
        let real_id = html_id.substring(0, html_id.length-"_drag".length) + "_init";
        return document.getElementById(real_id);
    }

    static get_html_node_from_inside_node(node) {
        while (!(node.id.startsWith("classe_classe_") && node.id.endsWith("_init"))) {
            node = node.parentElement;
        }
        return node;
    }

    static get_from_node_in_classes_tab(node) {
        return Section.get_by_id(node.id.split("_")[2]);
    }

    static are_in_the_same_niveau(node1, node2) {
        return (node1.parentElement === node2.parentElement);
    }
    
    /*
     * Retourne le noeud HTML de la section pour l'onglet des profs (préférences)
     */
    html_element_for_prefs_tab() {
        let lediv = document.createElement("DIV");
        lediv.id = "prof_section_" + this.id;
        lediv.classList.add("une_classe");
        lediv.innerHTML = this.nom + " (" + this.heures + ")";
        return lediv;
    }

    /*
     * Retourne l'id de la section à partir de l'id html des prefs
     */
    static get_id_from_html_id_prefs_tab(html_id) {
        return Number(html_id.substring("prof_section_".length));
    }
    
    /*
     * Retourne le nom affiché à l'écran
     */
    get_displayed_nom() {
        return document.getElementById("classe_classe_" + this.id + "_nom").value;
    }

    /*
     * Retourne le nombre d'heures affiché à l'écran dans l'onglet classes
     */
    get_displayed_heures() {
        return Number(document.getElementById("classe_classe_" + this.id + "_heures").value);
    }

    /*
     * Retourne le nombre de classes affiché à l'écran dans l'onglet classes
     */
    get_displayed_nb() {
        return Number(document.getElementById("classe_classe_" + this.id + "_nb").value);
    }

    /*
     * Retourne l'id du groupe sélectionné à l'écran dans l'onglet classes
     */
    get_displayed_groupe() {
        return Number(document.getElementById("classe_classe_" + this.id + "_groupe").value);
    }
    
    /*
     * actualise les groupes disponibles pour cette section dans l'onglet classes.
     * Si le groupe n'est pas celui par défaut, on l'ajoute
     * Si le groupe a disparu, on maj le valeur
     *
     * ids: les valeurs des options
     * noms: les noms des options à afficher
     * value: la valeur à sélectionner (si possible)
     */
    set_displayed_groupes(ids, noms, value) {
        let inner = "";
        if (this.groupe != this.id) {
            inner += "<option value='" + this.id + "'>" + this.nom + "</option>";
        }
        for (let k=0; k<ids.length; k++) {
            inner += "<option value='" + ids[k] + "'>" + noms[k] + "</option>";
        }
        let element = document.getElementById("classe_classe_" + this.id + "_groupe");
        element.innerHTML = inner;
        // Si la valeur n'est pas dans le tableau des ids: elle a été modifiée, on maj
        if (ids.indexOf(value) == -1) {
            this.groupe = value = Section.get_by_id(value).groupe;
        }
        element.value = value;
    }

    /*
     *
     */
    fill_services_tab(prof_containers, default_container, drop_callback) {
        for (let c=0; c<this.classes.length; c++) {
            let type_list = "verti";
            let container = default_container;
            if (this.classes[c].prof_id != -1) {
                container = prof_containers[this.classes[c].prof_id];
                type_list = "horiz";
            }
            let element = this.classes[c].html_element_for_services_tab();
            container.appendChild(element);
            TireEtLache.droppable(element, drop_callback, type_list);
            element.addEventListener('dblclick', this.mise_valeur_groupe);
        }
    }

    /*
     * Mise en valeur du groupe dans l'onglet Services
     */
    mise_valeur_groupe(event) {
        event.preventDefault();
        let mev = Classe.is_mise_valeur_from_html_node(event.target);
        Classe.remove_mise_valeur();
        if (mev) {
            let la_section = Section.get_by_id(Classe.get_id_section_from_html_id(event.target.id));
            let grp = la_section.groupe;
            let le_niveau = la_section.niveau;
            for (let s=0; s<le_niveau.sections.length; s++) {
                if (le_niveau.sections[s].groupe == grp) {
                    for (let c=0; c<le_niveau.sections[s].classes.length; c++) {
                        le_niveau.sections[s].classes[c].mise_valeur();
                    }
                }
            }
        }
    }
    
    /*
     * Sauve les données de la section depuis l'onglet des classes
     */
    save_me() {
        repartition.empile();
        this.nom = this.get_displayed_nom();
        this.heures = this.get_displayed_heures();
        this.groupe = this.get_displayed_groupe();
        this.nb = this.get_displayed_nb();
        while (this.nb < this.classes.length) {
            this.classes.pop();
        }
        while (this.nb > this.classes.length) {
            this.classes.push(new Classe(this, -1, this.classes.length));
        }
    }

}


/*
* Classe gérant une classe dans tous les onglets
*/
class Classe {
    constructor(section, prof_id, indice) {
        this.section = section;
        this.prof_id = prof_id;
        this.indice = indice
    }

    html_element_for_services_tab() {
        let lediv = document.createElement("DIV");
        lediv.id = "service_section_" + this.section.id + "_" + this.indice;
        lediv.classList.add("une_classe");
        lediv.innerHTML = this.section.nom + " (" + this.section.heures + ")";
        return lediv;
    }

    static get_by_html_id(html_id) {
        // html_id = "service_section_" + section.id + "_" + classe.ind
        let splitted = html_id.split("_");
        let la_section = Section.get_by_id(splitted[2])
        return la_section.classes[splitted[3]];
    }
    
    static is_mise_valeur_from_html_node(node) {
        return !node.classList.contains("classe_mev");
    }
    
    static remove_mise_valeur() {
        let mevliste = document.getElementsByClassName("classe_mev");
        // getElementsByClassName renvoie une HTMLCollection qui se modifie en live.
        while(mevliste.length > 0) {
            mevliste[0].classList.remove("classe_mev");
        }
    }

    static get_id_section_from_html_id(html_id) {
        return Number(html_id.split("_")[2]);
    }

    mise_valeur() {
        document.getElementById("service_section_" + this.section.id + "_" + this.indice).classList.add("classe_mev");
    }
}


/*
 * Classe gérant un prof dans tous les onglets
 */
class Prof {
    static max_id = 1;
    static object_list = {};

    static register(obj) {
        Prof.object_list[obj.id] = obj;
    }

    static unregister(id) {
        delete Prof.object_list[id];
    }

    static get_by_id(id) {
        if (id in Prof.object_list) return Prof.object_list[id];
        return null;
    }
    
    constructor(data=null) {
        if (data == null) {
            data = {"id":-1, "nom":"", "heure_min":15, "heure_max":20};
            data.affinite_simple = [];
            data.affinite_forte = [];
            data.classe_imposee = [];
        }
        this.id = data.id;
        this.nom = data.nom;
        this.heure_min = data.heure_min;
        this.heure_max = data.heure_max;
        this.affinite_simple = data.affinite_simple;
        this.affinite_forte = data.affinite_forte
        this.classe_imposee = data.classe_imposee;
        if (this.id < 0) {
            this.id = Prof.max_id;
            Prof.max_id++;
        } else {
            if (Prof.max_id <= this.id) {
                Prof.max_id = this.id + 1;
            }
        }
        Prof.register(this);
    }

    jsonify() {
        let data = {"id": this.id,
                    "nom": this.nom,
                    "heure_min": this.heure_min,
                    "heure_max": this.heure_max,
                    "affinite_simple": this.affinite_simple,
                    "affinite_forte": this.affinite_forte,
                    "classe_imposee": this.classe_imposee
                   };
        return data;
    }

    static focus_lost_in_profs_tab(prof_id) {
        Prof.get_by_id(prof_id).save_me();
    }

    
    /*
     * Retourne l'objet HTML représentant le prof pour l'onglet des profs
     */
    html_element_for_profs_tab() {
        let element = document.createElement("TR");
        let prof_p = "prof_prof_" + this.id;
        element.id = prof_p + "_init";
        let inner = "<td><img src='images/drag-reorder.png' alt='réordonner' width='18' id='" + prof_p + "_drag'></td>";
        inner += "<td><input id=\"" + prof_p + "_nom\" type=\"text\" value=\""+ this.nom + "\" onfocusout=\"Prof.focus_lost_in_profs_tab(" + this.id + ")\"></td>";
        inner += "<td><input class=\"nombre\" id=\"" + prof_p + "_min\" title=\"Minimum imposé par son status\" type=\"number\" step=\"any\" min=0 value=\"" + this.heure_min + "\" onfocusout=\"Prof.focus_lost_in_profs_tab(" + this.id + ")\"></td>";
        inner += "<td><input class=\"nombre\" id=\"" + prof_p + "_max\" title=\"Maximum souhaité par le prof\" type=\"number\" step=\"any\" min=0 value=\"" + this.heure_max + "\" onfocusout=\"Prof.focus_lost_in_profs_tab(" + this.id + ")\"></td>";
        inner += "<td><button type=\"button\" onclick='repartition.prefs_prof.show_prefs(" + this.id + ")' title='Accéder aux préférences de " + this.nom + "'><img src='images/prof-prefs.png' alt='Modifier les préférences' width='12'></button></td>";
        inner += "<td><button type=\"button\" onclick='repartition.profs_tab.remove_prof(" + this.id + ")' title='Supprimer ce prof'><img src='images/prof-remove.png' alt='Supprimer le prof' width='12'></button></td>";
        element.innerHTML = inner;
        return element;
    }

    get_html_draggable_element_in_profs_tab() {
        return document.getElementById("prof_prof_" + this.id + "_drag")
    }

    static get_html_node_from_html_drag_id(html_id) {
        let real_id = html_id.substring(0, html_id.length-"_drag".length) + "_init";
        return document.getElementById(real_id);
    }

    static get_html_node_from_inside_node(node) {
        while (!(node.id.startsWith("prof_prof_") && node.id.endsWith("_init"))) {
            node = node.parentElement;
        }
        return node;
    }

    static get_from_node_in_profs_tab(node) {
        return Prof.get_by_id(node.id.split("_")[2]);
    }
    
    html_element_for_services_tab() {
        let element = document.createElement("TR");
        element.id = "service_prof_" + this.id;
        element.classList.add("un_prof");
        let inner =  "<th>" + this.nom + "</th><td id=\"service_prof_" + this.id + "_heure\">";
        inner += "</td><td id=\"service_prof_" + this.id + "_classes\"></td>";
        element.innerHTML = inner;
        return element;
    }

    html_classes_container_in_services_tab() {
        return document.getElementById("service_prof_" + this.id + "_classes");
    }

    html_heure_container_in_services_tab() {
        return document.getElementById("service_prof_" + this.id + "_heure");
    }

    static get_id_from_html_id(html_id) {
        // html_id = "service_prof_" + id
        return Number(html_id.split("_")[2]);
    }
    /*
     *
     */
    maj_heures_service(lesheures) {
        let heure_prof = 0;
        // Calcul des heures profs à partir des heures élèves.
        // En Seconde : pas de pondération
        // En 1ère et Tal : pondération 1.1 dans la limite de 10h (soit 1h sup maxi)
        // En Supérieur BTS: pondération 1.25
        // En Supérieur CPGE: pondération 1.5
        
        heure_prof += lesheures["2nd"];
        heure_prof += 1.1*Math.min(10,lesheures["tal"]) + Math.max(0, lesheures["tal"]-10);
        heure_prof += 1.25*lesheures["bts"];
        heure_prof += 1.5*lesheures["cpge"];
        heure_prof = Math.round(100*heure_prof)/100;
        let hpe = lesheures["2nd"] + lesheures["tal"] + lesheures["bts"] + lesheures["cpge"];
        hpe = Math.round(100*hpe)/100;
        
        document.getElementById("service_prof_" + this.id + "_heure").innerHTML = "(" + hpe + ") " + heure_prof + "/" + this.heure_min;
        // coloration du prof en fonction de ses heures
        let leprof = document.getElementById("service_prof_" + this.id);
        if (heure_prof <this.heure_min) {
            leprof.classList.add("peu_heure");
            leprof.classList.remove("bonne_heure");
            leprof.classList.remove("trop_heure");
        } else if (heure_prof <= this.heure_max) {
            leprof.classList.remove("peu_heure");
            leprof.classList.add("bonne_heure");
            leprof.classList.remove("trop_heure");
        } else {
            leprof.classList.remove("peu_heure");
            leprof.classList.remove("bonne_heure");
            leprof.classList.add("trop_heure");
        }
    }
    
    /*
     * Retourne le nom affiché à l'écran dans l'onglet Profs
     */
    get_displayed_nom() {
        return document.getElementById("prof_prof_" + this.id + "_nom").value;
    }

    /*
     * Retourne le nombre d'heure min affiché à l'écran dans l'onglet Profs
     */
    get_displayed_heures_min() {
        return Number(document.getElementById("prof_prof_" + this.id + "_min").value);
    }

    /*
     * Retourne le nombre d'heure max affiché à l'écran dans l'onglet Profs
     */
    get_displayed_heures_max() {
        return Number(document.getElementById("prof_prof_" + this.id + "_max").value);
    }

    /*
     * Sauve les données depuis l'onglet Profs
     */
    save_me() {
        repartition.empile();
        this.nom = this.get_displayed_nom();
        this.heure_min = this.get_displayed_heures_min();
        this.heure_max = this.get_displayed_heures_max();
    }
}


//
//
// Classes de gestion d'interface
//
//



/*
 * Classe gérant l'onglet de modification des classes
 */
class ClassesTab extends GeneralTab {
    constructor(repart) {
        super("modif_classes", "classes_but", null, {"active":"active_but", "passive":"passive_but"});
        this.repart = repart;
        this.liste_niveaux = document.getElementById("classe_liste_niveaux");
    }
    
    init_tab() {
        this.liste_niveaux.innerHTML = "";
        for (let k=0; k<this.repart.niveaux.length; k++) {
            this.liste_niveaux.appendChild(this.repart.niveaux[k].html_element_for_classes_tab());
            let dzone = this.repart.niveaux[k].get_dropzone_for_classes_tab();
            TireEtLache.dropzone(dzone, this.drop_section_here, false);
            for (let s=0; s<this.repart.niveaux[k].sections.length; s++) {
                dzone.appendChild(this.repart.niveaux[k].sections[s].html_element_for_classes_tab());
                TireEtLache.droppable(this.repart.niveaux[k].sections[s].get_html_draggable_element_in_classes_tab(), this.drop_section_here, "verti");
            }
            this.maj_groupes(this.repart.niveaux[k].id, true);
        }
    }

    /*
     * Mis à jour des groupes disponibles pour les sections du niveau correspondant
     */
    maj_groupes(niv_id, init=false) {
        // repérage du bon niveau
        let le_niveau = Niveau.get_by_id(niv_id);
        // préparation des groupes communs
        let noms = [];
        let ids = [];
        let values = [];
        let val, nom, la_section;
        for (let s=0; s<le_niveau.sections.length; s++) {
            la_section = le_niveau.sections[s];
            val = la_section.groupe;
            nom = la_section.nom;
            if (!init) {
                la_section.groupe = val = la_section.get_displayed_groupe();
                la_section.nom    = nom = la_section.get_displayed_nom();
            }
            if (Section.get_by_id(val) == null) { la_section.groupe = val = la_section.id; }
            values.push(val);
            if (val == la_section.id) {
                noms.push(nom);
                ids.push(la_section.id);
            }
        }
        // maj des groupes
        for (let s=0; s<le_niveau.sections.length; s++) {
            le_niveau.sections[s].set_displayed_groupes(ids, noms, values[s]);
        }
    }
    
    add_niveau() {
        this.save_me();
        let niv = new Niveau()
        this.repart.niveaux.push(niv);
        this.liste_niveaux.appendChild(niv.html_element_for_classes_tab());
        TireEtLache.dropzone(niv.get_dropzone_for_classes_tab(), this.drop_section_here, false);
        this.add_section(niv.id);
    }

    remove_niveau(n) {
        let callback = function(this_object) {
            return function() {
                this_object.save_me();
                let ind = -1;
                for (let k=0; k<this_object.repart.niveaux.length; k++) {
                    if (this_object.repart.niveaux[k].id == n) {
                        ind = k;
                        break;
                    }
                }
                this_object.liste_niveaux.removeChild(this_object.liste_niveaux.childNodes[ind]);
                this_object.repart.niveaux.splice(ind,1);
                Niveau.unregister(n);
            };
        };
        this.repart.confirme_tab.active("ce niveau", callback(this));
    }
    
    add_section(niv_id) {
        this.save_me();
        let le_niveau = Niveau.get_by_id(niv_id);
        let sect = new Section(le_niveau);
        le_niveau.add_section(sect);
        let dzone = le_niveau.get_dropzone_for_classes_tab();
        dzone.appendChild(sect.html_element_for_classes_tab());
        TireEtLache.droppable(sect.get_html_draggable_element_in_classes_tab(), this.drop_section_here, "verti");
    }

    remove_section(sec_id) {
        let callback = function(this_object) {
            return function() {
                this_object.save_me();
                let la_section = Section.get_by_id(sec_id);
                la_section.niveau.remove_section(la_section);
            };
        };
        this.repart.confirme_tab.active("cette section", callback(this));
    }

    drop_section_here(event, dropped_element, target_dropzone, target_draggable) {
        let dropped = Section.get_html_node_from_html_drag_id(dropped_element.id)
        let x = event.clientX, y = event.clientY;
        let target = Section.get_html_node_from_inside_node(document.elementFromPoint(x,y));
        if (!Section.are_in_the_same_niveau(dropped, target)) { return; }
        let lieu = "beforebegin";
        if (target.previousSibling === dropped) {
            lieu = "afterend";
        }
        repartition.empile();
        target.insertAdjacentElement(lieu, dropped);
        // très sale mais pas de this...
        repartition.classes_tab.save_me();
    }
    
    save_me() {
        this.repart.empile();
        for (let k=0; k<this.repart.niveaux.length; k++) {
            this.repart.niveaux[k].save_me();
        }
    }
    
}




/*
 * Classe gérant l'onglet de modification des profs
 */
class ProfsTab extends GeneralTab {
    constructor(tab, repart) {
        super("modif_profs", "profs_but", [tab], {"active":"active_but", "passive":"passive_but"});
        this.repart = repart;
        this.liste_profs = document.getElementById("prof_liste_profs");
        TireEtLache.dropzone(this.liste_profs, this.drop_prof_here, false);
    }
    
    init_tab() {
        this.liste_profs.innerHTML = "";
        for (let p=0; p<this.repart.profs.length; p++) {
            this.liste_profs.appendChild(this.repart.profs[p].html_element_for_profs_tab());
            TireEtLache.droppable(this.repart.profs[p].get_html_draggable_element_in_profs_tab(), this.drop_prof_here, "verti");
        }
    }

    add_prof() {
        this.save_me();
        let prf = new Prof();
        this.repart.profs.push(prf);
        this.liste_profs.appendChild(prf.html_element_for_profs_tab());
        TireEtLache.droppable(prf.get_html_draggable_element_in_profs_tab(), this.drop_prof_here, "verti");
    }

    remove_prof(p) {
        let callback = function(this_object) {
            return function() {
                this_object.save_me();
                let ind = -1;
                for (let k=0; k<this_object.repart.profs.length; k++) {
                    if (this_object.repart.profs[k].id == p) {
                        ind = k;
                        break;
                    }
                }
                this_object.liste_profs.removeChild(this_object.liste_profs.childNodes[ind]);
                this_object.repart.profs.splice(ind,1);
                this_object.repart.prof_removed(p);
                Prof.unregister(p);
            };
        };
        this.repart.confirme_tab.active("ce prof", callback(this));
    }

    drop_prof_here(event, dropped_element, target_dropzone, target_draggable) {
        repartition.empile();
        let dropped = Prof.get_html_node_from_html_drag_id(dropped_element.id)
        let x = event.clientX, y = event.clientY;
        let target = Prof.get_html_node_from_inside_node(document.elementFromPoint(x,y));
        let lieu = "beforebegin";
        if (target.previousSibling === dropped) {
            lieu = "afterend";
        }
        target.insertAdjacentElement(lieu, dropped);
        // très sale mais pas de this...
        repartition.profs_tab.save_me();
    }
        
    save_me() {
        this.repart.empile();
        for (let k=0; k<this.repart.profs.length; k++) {
            this.repart.profs[k].save_me();
        }
        let newliste = [];
        for (let k=0; k<this.liste_profs.childElementCount; k++) {
            newliste.push(Prof.get_from_node_in_profs_tab(this.liste_profs.children[k]));
        }
        this.repart.profs = newliste;
    }

}


/*
 * classe gérant les préférences des profs
 */
class PrefsProfTab extends GeneralTab {
    constructor(repart) {
        super("preference_prof", "add_prof_button");
        this.repart = repart;
        this.current_prof = null;
        this.nom = document.getElementById("prof_pref_nom");
        this.classes_libres = document.getElementById("prof_classes_libres");
        this.affinite_forte = document.getElementById("prof_affinite_forte");
        this.affinite_simple = document.getElementById("prof_affinite_simple");
        this.classes_imposees = document.getElementById("prof_classes_imposees");
        TireEtLache.dropzone(this.classes_libres, this.drop_in_pref_tab);
        TireEtLache.dropzone(this.affinite_forte, this.drop_in_pref_tab);
        TireEtLache.dropzone(this.affinite_simple, this.drop_in_pref_tab);
        TireEtLache.dropzone(this.classes_imposees, this.drop_in_pref_tab);
    }

    init_tab() {
        this.nom.innerHTML = this.current_prof.nom;
        this.classes_libres.innerHTML = "";
        this.affinite_forte.innerHTML = "";
        this.affinite_simple.innerHTML = "";
        this.classes_imposees.innerHTML = "";
        for (let n=0; n<this.repart.niveaux.length; n++) {
            for (let s=0; s<this.repart.niveaux[n].sections.length; s++) {
                let element = this.repart.niveaux[n].sections[s].html_element_for_prefs_tab();
                TireEtLache.droppable(element, this.drop_in_pref_tab, "verti");
                let container = this.classes_libres;
                let k = this.repart.niveaux[n].sections[s].id;
                if (this.current_prof.affinite_forte.indexOf(k) != -1) { container = this.affinite_forte; }
                else if (this.current_prof.affinite_simple.indexOf(k) != -1) { container = this.affinite_simple; }
                else if (this.current_prof.classe_imposee.indexOf(k) != -1) { container = this.classes_imposees; }
                container.appendChild(element);
            }
        }
    }

    /*
     * initialise le prof et lance l'affichage.
     */
    show_prefs(p) {
        this.repart.profs_tab.save_me();
        this.current_prof = Prof.get_by_id(p);
        if (this.current_prof != null) {
            this.show_me(true);
        }
    }

    /*
     * retourne un tableau constitué des id des sections à partir du noeud html
     */
    static rempli(htmlDiv) {
        let array = [];
        let children = htmlDiv.children;
        for (let c=0; c<children.length; c++) {
            let k = Section.get_id_from_html_id_prefs_tab(children[c].id);
            array.push(k);
        }
        return array;
    }

    save_me() {
        if (this.current_prof == null) return;
        this.repart.empile();
        this.current_prof.affinite_forte = PrefsProfTab.rempli(this.affinite_forte);
        this.current_prof.affinite_simple = PrefsProfTab.rempli(this.affinite_simple);
        this.current_prof.classe_imposee = PrefsProfTab.rempli(this.classes_imposees);
    }

    /*
     * ce qu'il faut faire quand on drop un element (section) dans une zone (affinité, rejet,...)
     */
    drop_in_pref_tab(event, dropped_element, target_dropzone, target_draggable) {
        if (dropped_element.parentElement !== target_dropzone) {
            target_dropzone.appendChild(dropped_element);
        }
    }

}


/*
 * Classe gérant l'onglet des services
 */
class ServicesTab extends GeneralTab {
    constructor(repart) {
        super("modif_services", "services_but", null, {"active":"active_but", "passive":"passive_but"});
        this.repart = repart;
        this.default_dropzone_niveau = document.getElementById("service_niveaux");
        TireEtLache.dropzone(this.default_dropzone_niveau, this.drop_in_service_tab);
    }

    init_tab() {
        let les_profs_html = document.getElementById("service_profs");
        les_profs_html.innerHTML = "";
        let prof_containers = {};
        for (let p=0; p<this.repart.profs.length; p++) {
            let le_prof = this.repart.profs[p];
            let element = le_prof.html_element_for_services_tab();
            les_profs_html.appendChild(element);
            TireEtLache.dropzone(element, this.drop_in_service_tab);
            prof_containers[le_prof.id] = le_prof.html_classes_container_in_services_tab();
        }
        let html_nom_niveaux = document.getElementById("service_nom_niveaux");
        html_nom_niveaux.innerHTML = "";
        this.default_dropzone_niveau.innerHTML = "";
        for (let n=0; n<this.repart.niveaux.length; n++) {
            let le_niveau = this.repart.niveaux[n];
            html_nom_niveaux.appendChild(le_niveau.html_name_element_for_services_tab());
            let default_container = le_niveau.html_container_element_for_services_tab();
            this.default_dropzone_niveau.appendChild(default_container);
            le_niveau.fill_services_tab(prof_containers, default_container, this.drop_in_service_tab);
        }
        this.maj_stats();
    }

    /*
     *
     */
    maj_stats() {
        // mise à jour des heures par prof et des stats
        let lesheures = {};
        let totalh = 0;
        let totalr = 0;
        for (let p=0; p<this.repart.profs.length; p++) { lesheures[this.repart.profs[p].id] = {"2nd":0, "tal":0, "bts":0, "cpge":0}; }
        for (let n=0; n<this.repart.niveaux.length; n++) {
            let le_niveau = this.repart.niveaux[n];
            let le_poids = le_niveau.poids;
            for (let s=0; s<le_niveau.sections.length; s++) {
                let la_section = le_niveau.sections[s];
                totalh += la_section.heures * la_section.nb;
                for (let c=0; c<la_section.classes.length; c++) {
                    if (la_section.classes[c].prof_id == -1) {
                        totalr += la_section.heures;
                    } else {
                        lesheures[la_section.classes[c].prof_id][le_poids] += la_section.heures;
                    }
                }
            }
        }
        // maj des nombres d'heures
        for (let p=0; p<this.repart.profs.length; p++) {
            this.repart.profs[p].maj_heures_service(lesheures[this.repart.profs[p].id]);
        }
        // maj des stats
        document.getElementById("service_heure_total").innerHTML = totalh;
        let totalp = 0;
        for (let p=0; p<this.repart.profs.length; p++) { totalp += this.repart.profs[p].heure_min; }
        document.getElementById("service_heure_prof").innerHTML = totalp;
        document.getElementById("service_heure_reste").innerHTML = totalr;
    }

    /*
     *
     */
    drop_in_service_tab(event, dropped_element, target_dropzone, target_draggable) {
        let parent = dropped_element.parentElement;
        let dropzone_niveaux = document.getElementById("service_niveaux");
        let maj = false;
        
        if (target_draggable === dropped_element) { return; }
        let n = Section.get_by_id(Classe.get_id_section_from_html_id(dropped_element.id)).niveau.id;
        let niveau_container = document.getElementById("service_niveau_" + n);
        let prof_container = document.getElementById(target_dropzone.id + "_classes");
        if (target_draggable === null) {
            // on drop directement sur une dropzone.
            let new_container = (target_dropzone == dropzone_niveaux ? niveau_container : prof_container);
            let first_element = new_container.firstElementChild;
            let last_element = new_container.lastElementChild;
            if (first_element === null) {
                new_container.appendChild(dropped_element);
                maj = true;
            } else if (event.offsetY < first_element.offsetTop) {
                if (dropped_element !== first_element) {
                    new_container.insertBefore(dropped_element, first_element);
                    maj = true;
                }
            } else if (dropped_element !== last_element) {
                new_container.appendChild(dropped_element);
                maj = true;
            }
        } else {
            // on drop sur un draggable.
            if (dropzone_niveaux.contains(target_draggable)) {
                // target_draggable in niveaux
                if (dropzone_niveaux.contains(dropped_element)) {
                    // target_draggable in niveaux , element in niveaux
                    if (niveau_container.contains(target_draggable)) {
                        let lieu = (target_draggable.previousSibling === dropped_element ? "afterend" : "beforebegin");
                        target_draggable.insertAdjacentElement(lieu, dropped_element);
                        maj = true;
                    }
                } else {
                    // target_draggable = niveaux , element in profs
                    maj = true;
                    if (niveau_container.contains(target_draggable)) {
                        target_draggable.insertAdjacentElement("beforebegin", dropped_element);
                    } else {
                        niveau_container.appendChild(dropped_element);
                    }
                }
            } else {
                // target_draggable in profs
                let lieu = "beforebegin";
                if (!dropzone_niveaux.contains(dropped_element) && target_draggable.previousSibling === dropped_element) {
                    lieu = "afterend";
                }
                target_draggable.insertAdjacentElement(lieu, dropped_element);
                maj = true;
            }
        }
        if (maj) {
            if (target_dropzone == dropzone_niveaux) {
                dropped_element.classList.remove("horizontal_list");
                dropped_element.classList.add("vertical_list");
            } else {
                dropped_element.classList.remove("vertical_list");
                dropped_element.classList.add("horizontal_list");
            }
            // là, c'est horriblement moche !
            // Dans l'idéal, il faudrait utiliser 'this' au lieu de 'repartition', mais il est 'undefined'
            repartition.empile();
            let la_classe = Classe.get_by_html_id(dropped_element.id);
            if (target_dropzone == dropzone_niveaux) {
                la_classe.prof_id = -1;
            } else {
                la_classe.prof_id = Prof.get_id_from_html_id(target_dropzone.id);
            }
            repartition.services_tab.save_me();
            repartition.services_tab.maj_stats();
            repartition.unredo.rebut.disabled = true;
        }
    }

}




class SauveTab extends GeneralTab {
    constructor(repart) {
        super("sauve_json", "sauve_json_button");
        this.repart = repart;
        this.dl_element = document.getElementById("download_link");
    }

    init_tab() {
        document.getElementById("sauve_json_name").value = this.repart.nom;
    }

    go_download(texte, mimetype, filename) {
        this.dl_element.setAttribute("href", "data:"+mimetype+";charset=utf-8," + encodeURIComponent(texte));
        this.dl_element.setAttribute("download", filename.replace(" ", "_"));
        this.dl_element.click();
    }

    click_save() {
        if (document.getElementById("csv_radio").checked) { this.export_csv(); }
        if (document.getElementById("json_radio").checked) { this.export_json(); }
        this.show_me(false, false);
    }
    
    export_json() {
        // on sauve d'abord l'état actuel avant de l'enregistrer
        if (repartition.classes_tab.is_shown()) { repartition.classes_tab.save_me(); }
        if (repartition.profs_tab.is_shown()) { repartition.profs_tab.save_me(); }
        if (repartition.services_tab.is_shown()) { repartition.services_tab.save_me(); }
        let nom = document.getElementById("sauve_json_name").value;
        repartition.set_nom(nom);
        // on prépare le fichier
        let texte = JSON.stringify(repartition.jsonify());
        let nomfic = nom + ".json";
        // on le soumet au téléchargement
        this.go_download(texte, "application/json", nomfic);
    }

    export_csv() {
        // hack pour avoir les infos
        if (!repartition.services_tab.is_shown()) {
            repartition.onglets.select_tab(2);
        }
        // on exporte
        let texte = "\"Nom\";\"Heures statutaires\";\"Heures effectives\";\"Heures pondérées\";\"Classes\"\n";
        for (let p=0; p<this.repart.profs.length; p++) {
            let leprof = this.repart.profs[p];
            let idp = leprof.id;
            let heure = document.getElementById("service_prof_" + idp + "_heure").innerText;
            let stat = heure.split("/")[1].replace(".", ",");
            let effect = heure.split("(")[1].split(")")[0].replace(".", ",");
            let pond = heure.split(" ")[1].split("/")[0].replace(".", ",");
            texte += "\"" + leprof.nom + "\";" + stat + ";" + effect + ";" + pond;
            let k = 0;
            for (let n=0; n<this.repart.niveaux.length; n++) {
                for (let s=0; s<this.repart.niveaux[n].sections.length; s++) {
                    for (let c=0; c<this.repart.niveaux[n].sections[s].classes.length; c++) {
                        if (this.repart.niveaux[n].sections[s].classes[c].prof_id == leprof.id) {
                            texte += ";" + this.repart.niveaux[n].sections[s].nom;
                        }
                    }
                }
            }
            texte += "\n";
        }
        let nomfic = this.repart.nom + ".csv";
        this.go_download(texte, "text/csv", nomfic);
    }
 
}


class ChargeTab extends GeneralTab {
    constructor() {
        super("charge_json", "open_json");
        this.filenode = document.getElementById("charge_json_name");
    }

    init_tab() {
    }

    click_load() {
        let fichier = this.filenode.files[0];
        if (fichier == null) { return; }
        if (fichier.type && !fichier.type.endsWith("/json")) { return; }
        let reader = new FileReader();
        reader.addEventListener('load', function(event) {
            let data = event.target.result;
            repartition.recharge(JSON.parse(data));
        });
        reader.readAsText(fichier);
        repartition.charger_tab.show_me(false);
    }

    async click_load_test() {
        const request = new Request("repart-data.json");
        const response = await fetch(request);
        const data = await response.json();
        repartition.recharge(data);
        repartition.charger_tab.show_me(false);
    }
}


class ConfirmeTab extends GeneralTab {
    constructor() {
        super("confirme_remove");
        this.label = document.getElementById("confirme_nature");
        this.action = null;
    }

    active(nature, callback) {
        this.label.innerHTML = nature;
        this.action = callback;
        this.show_me(true);
    }

    click_ok() {
        if (this.action != null) this.action();
        this.show_me(false);
    }

}


class ConflictsTab extends GeneralTab {
    constructor(repart) {
        super("show_conflicts");
        this.repart = repart;
        this.table = document.getElementById("conflicts_table");
    }

    init_tab() {
        let latable = "<tr><th>Classe</th><th>Cause</th><th>nb de classes dispos</th><th>Profs en cause</th></tr>";
        let impo = "";
        let frt = "";
        let fbl = "";
        for (let k=0; k<this.repart.conflicts.length; k++) {
            let n = this.repart.conflicts[k][0];
            let s = this.repart.conflicts[k][1];
            let barre_init = "";
            let barre_fini = "";
            let nb = 0;
            for (let j=0; j<this.repart.niveaux[n].sections[s].classes.length; j++) {
                if (this.repart.niveaux[n].sections[s].classes[j].prof_id == -1) {
                    nb += 1;
                }
            }
            if (nb == 0) {
                barre_init = "<span class='prb_regle'>";
                barre_fini = "</span>";
            }
            let msg = ""
            msg += "<tr>";
            msg += "<td>" + barre_init + this.repart.niveaux[n].sections[s].nom + barre_fini + "</td>";
            msg += "<td>" + barre_init + this.repart.conflicts[k][2] + barre_fini + "</td>";
            msg += "<td>" + barre_init + this.repart.conflicts[k][3] + barre_fini + "</td>";
            let ppfr = "";
            for (let j=0; j<this.repart.conflicts[k][4].length; j++) {
                if (j > 0) { ppfr += ", "; }
                let p = this.repart.conflicts[k][4][j];
                ppfr += this.repart.profs[p].nom;
            }
            msg += "<td>" + barre_init + ppfr + barre_fini + "</td>";
            msg += "</tr>";
            if (this.repart.conflicts[k][2] == "imposée") { impo += msg; }
            if (this.repart.conflicts[k][2] == "forte") { frt += msg; }
            if (this.repart.conflicts[k][2] == "faible") { fbl += msg; }
        }
        latable += impo + frt + fbl;
        this.table.innerHTML = latable;
    }
}




/**************\
 * main class *
\**************/
class Repartition {
    constructor() {
        
        // les onglets
        this.classes_tab = new ClassesTab(this);
        
        this.prefs_prof = new PrefsProfTab(this);
        this.profs_tab = new ProfsTab(this.prefs_prof, this);
        
        this.services_tab = new ServicesTab(this);
        
        this.charger_tab = new ChargeTab();
        this.sauver_tab = new SauveTab(this);
        
        this.conflicts_tab = new ConflictsTab(this);
        
        this.unredo = new UndoRedo("undo_button", "redo_button");
        this.confirme_tab = new ConfirmeTab();
        
        this.onglets = new MultiTab([this.classes_tab, this.profs_tab, this.services_tab]);

        // les données
        this.nom = "";
        this.profs = [];
        this.niveaux = [];
        this.conflicts = [];
    }

    jsonify() {
        let data = {"nom": this.nom, "niveaux":[], "profs":[]};
        for (let n=0; n<this.niveaux.length; n++) {
            data.niveaux.push(this.niveaux[n].jsonify());
        }
        for (let p=0; p<this.profs.length; p++) {
            data.profs.push(this.profs[p].jsonify());
        }
        return data;
    }

    set_nom(nom) {
        this.nom = nom;
        document.getElementById("global_name").innerHTML = this.nom;
    }
    
    empile() {
        if (this.unredo.empile(this.jsonify())) {
            this.unredo.rebut.disabled = true;
        }
    }

    undo() {
        let data = this.unredo.undo(this.jsonify());
        this.recharge(data, false);
    }

    redo() {
        let data = this.unredo.redo();
        this.recharge(data);
    }

    recharge(data, savestate=true) {
        // on efface tout
        for (let k=0; k<this.niveaux.length; k++) { Niveau.unregister(this.niveaux[k].id); }
        for (let k=0; k<this.profs.length; k++) { Prof.unregister(this.profs[k].id); }
        
        // on recommence
        this.profs = [];
        if (data == null) { return; }
        for (let k=0; k<data.profs.length; k++) {
            let prf = new Prof(data.profs[k]);
            this.profs.push(prf);
        }
        
        this.niveaux = [];
        for (let k=0; k<data.niveaux.length; k++) {
            let niv = new Niveau(data.niveaux[k]);
            this.niveaux.push(niv);
        }
        
        this.set_nom(data.nom);

        this.onglets.recharge();
        if (savestate) { this.empile(); }
    }

    prof_removed(prof_id) {
        for (let n=0; n<this.niveaux.length; n++) {
            for (let s=0; s<this.niveaux[n].sections.length; s++) {
                for (let c=0; c<this.niveaux[n].sections[s].classes.length; c++) {
                    if (this.niveaux[n].sections[s].classes[c].prof_id == prof_id) {
                        this.niveaux[n].sections[s].classes[c].prof_id = -1;
                    }
                }
            }
        }
    }
    
}







/**********************************************************************************\
 **********************************************************************************
 **********************************************************************************
 **********************************************************************************
 **********************************************************************************
\**********************************************************************************/

var repartition;

function init_everything() {
    repartition = new Repartition();
    repartition.onglets.select_tab(0);
    repartition.charger_tab.show_me(true);
    if (window.location.hostname == "vd_prof.forge.apps.education.fr") {
        console.log("données test disponibles.");
        let element = document.getElementById("charge-test");
        element.innerHTML = "<button class='a_droite' type='button' onclick='repartition.charger_tab.click_load_test()'>Données de test</button>";
    }
}



/**
 ** Gestion des boutons
 **/


function show_hide_infos() {
    document.getElementById("show_hide_info").classList.toggle("enfonce");
    let elements = document.getElementsByClassName("nofs");
    for (let i=0; i<elements.length; i++) {
        elements[i].classList.toggle("passive_tab");
    }
}

function click_restore() {
    repartition.services_tab.init_tab();
}

function click_empty_fill(sauve_etat=true) {
    repartition.conflicts = [];
    // on copie puis on efface ce qui doit.
    let nclasses = [];
    for (let n=0; n<repartition.niveaux.length; n++) {
        nclasses.push(repartition.niveaux[n].jsonify());
    }
    for (let n=0; n<repartition.niveaux.length; n++) {
        for (let s=0; s<repartition.niveaux[n].sections.length; s++) {
            for (let c=0; c<repartition.niveaux[n].sections[s].classes.length; c++) {
                repartition.niveaux[n].sections[s].classes[c].prof_id = -1;
            }
        }
    }
    if (sauve_etat) {
        repartition.services_tab.init_tab();
        repartition.empile();
    }
}

function click_see_conflicts() {
    repartition.conflicts_tab.show_me(true);
    console.log(repartition.conflicts);
}


function click_pre_fill() {
    click_empty_fill(false)
    // on tente de remplir
    for (let n=0; n<repartition.niveaux.length; n++) {
        for (let s=0; s<repartition.niveaux[n].sections.length; s++) {
            let lid = repartition.niveaux[n].sections[s].id;
            let pfort = [];
            let psimple = [];
            let pimpose = [];
            for (let p=0; p<repartition.profs.length; p++) {
                if (repartition.profs[p].classe_imposee.indexOf(lid) != -1) { pimpose.push(p); }
                else if (repartition.profs[p].affinite_forte.indexOf(lid) != -1) { pfort.push(p); }
                else if (repartition.profs[p].affinite_simple.indexOf(lid) != -1) { psimple.push(p); }
            }
            let nb = repartition.niveaux[n].sections[s].nb
            let rempli = function(tab, texte, indice) {
                if (tab.length > nb - indice) {
                    let message = [n, s, texte, nb-indice]
                    let prbs = [];
                    for (let k=0; k<tab.length; k++) {
                        prbs.push(tab[k]);
                    }
                    message.push(prbs)
                    repartition.conflicts.push(message);
                    return -1;
                }
                for (let k=0; k<tab.length; k++) {
                    repartition.niveaux[n].sections[s].classes[indice].prof_id = repartition.profs[tab[k]].id;
                    indice++;
                }
                return indice;
            };
            let ind = rempli(pimpose, "imposée", 0);
            if (ind > -1) {
                ind = rempli(pfort, "forte", ind);
                if (ind > -1)
                    rempli(psimple, "faible", ind);
            }
        }
    }
    repartition.services_tab.init_tab();
    repartition.empile();
}


/**
 ** hack pour impression
 **/
window.onbeforeprint = function(evt) {
    if (document.getElementById("show_hide_info").classList.contains("enfonce")) {
        show_hide_infos();
    }
    if (repartition.services_tab.is_shown()) {
    } else {
        repartition.onglets.select_tab(2);
    }
}


/**
 ** Fin du script
 **/
