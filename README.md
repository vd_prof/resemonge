# ReSeMonge

Aide à la répartition des services des profs de Maths de Monge.


## Mode d'emploi

1) Téléchargez l'ensemble des fichiers dans un seul répertoire.
2) ouvrez la page `repart.html` avec votre navigateur web préféré.

Tout se passe ne local, pas besion de réseau.


## Utilisation

Initialement, la page propose d'ouvrir un fichier de données. Cliquez sur Annuler pour partir d'une page vierge.

La page présente alors 3 onglets : **Classes**, **Profs** et **Services**.

### Onglet Classes

Il permet d'ajouter/supprimer/modifier des niveaux et des classes.

Un niveau est par exemple "Seconde", "Première", "BTS",...

Le _type de niveau_ permet de détermier le type de pondération des heures.

À l'intérieur d'un niveau, on peut ajouter des classes, avec un nom, un nombre d'heures devant élèves et le nombre de classes de ce type.
On peut aussi spécifier un _groupe_ à une classe.
Ceci permet de regrouper des classes identiques qui n'auraient pas le même nombre d'heures-élèves (par exemple si une classe a un effectif trop faible pour dédoubler ses heures de TD).
Ce groupe pourra être mis en valeur dans l'onglet **Services**.

À l'intérieur d'un niveau, on peut réorganiser les classes par drag n' drop sur l'icone à gauche de la classe.

### Onglet Profs

Cet onglet permet d'ajouter/supprimer/modifier des profs.

Outre son nom, on peut spécifier le nombre d'heure minimum qu'il doit faire (heures statutaires) ainsi que le nombre d'heures maximal qu'il souhaite faire
(en comptant les pondérations, les heures supplémentaires,...)

Dans les préférences du prof, on peut spécifier s'il a des classes imposées (par exemple pour les stagiaires). 
On peut aussi préciser deux types de préférences: l'affinité forte et l'affinité faible.
En gros, l'affinité forte est pour dire "Je veux vraiment cette classe!" et l'affinité faible serait plutôt
"J'aimerai bien cette classe...". Les autres classes sont neutres.

Historiquement, il y avait un algorithme qui essayait de répartir les classes au mieux. Il n'était pas très efficace, je l'ai enlevé mais
j'ai gardé ces deux types d'affinité. Elles peuvent servir dans l'onglet **Services**.

Ces préférences se manipulent au drag n' drop.

On peut réorganiser l'ordre des profs par drag n' drop sur l'icone à gauche.


### Onglet Services

Ce troisième onglet est le principal. Tout se passe en drag n' drop.

On bouge des classes de la zone des _classes non attribuées_ vers un prof ou d'un prof vers un autre.

Si un prof n'a pas atteint son nombre d'heures minimal, il reste en rouge.
Si c'est bon, il passe en vert, si c'est trop, il passe en gris.

Les couleurs sont plutôt pastelles. Elles ont été choisies pour que ça rende le mieux possible au vidéo-projecteur.

Double-cliquer sur une classe met en évidence toutes les classes du même groupe. Ceci permet de visualiser rapidement où on en est.
Pour enlever la mise en valeur, soit on double-clique à nouveau sur la classe, soit on double-clique sur une classe d'un autre groupe.

En bas, on trouve trois boutons: **Vider**, **Pré-remplir**, **Voir problèmes**.

Le premier remet toutes les classes dans la zone non attribuée.

Le deuxième permet de pré-affecter les classes qui ne posent aucun problème: on répartie d'abord les classes aux profs qui ont une affinité forte dessus.
S'il n'y a pas assez de classes, aucune classe n'est attribuée et ceci crée un "problème". 
S'il reste des classes, elles sont réparties entre les profs qui ont une affinité faible dessus, avec création d'un problème en cas de pénurie.
Les classes qui restent ne sont tout simplement pas affectées.

Le troisième bouton permet de voir tous les "problèmes" crées par le remplissage automatique, avec les profs qui sont concernés et le type d'affinité.


### Boutons Annuler/Refaire

Relativement évident: permet d'annuler une action ou la refaire.

### Bouton Plein-Ecran

Ce bouton permet d'enlever des explications et des choses jugées inutiles pour maximiser la place disponible pour les zones d'action.
C'est utile dans le cas où le vidéo-projecteur n'a pas une grosse résolution.

### Bouton Charger

Il permet de charger un jeu de données. De cette façon, on peut préparer la réunion en demandant préalablement à chaque prof
ses préférences (heures/classes) et à l'Administration les effectifs des classes et les nombres d'heures.
On gagne ainsi un temps précieux !

### Bouton Enregistrer

Deux options sont possibles ici: La première permet d'enregistrer les données au format json. C'est ce format qui est demandé par le bouton Charger.
C'est un format raisonnable à lire pour un humain comme pour un ordinateur.

L'autre option permet d'exporter au format CSV. C'est un format texte compatible avec un tableur.
C'était une demande du prof référent pour transmettre nos concertations à l'Administration.

### Impression

Lors de l'impression, les trucs inutiles sont supprimés.





## FAQ


 - Que signifie ReSeMonge ?
 Ça signifie ***Ré***partition de ***Se***rvices pour ***Monge***.
 Par ce que j'enseigne au lycée Monge à Chambéry.

 - Pourquoi l'avoir fait ?
 Au tout début, on faisait la répartition au tableau avec une craie, puis un feutre. Ce n'était pas pratique.
 Puis est venue l'ère du tableur. C'était déjà mieux mais toujours pas pratique:
 Les cases étaient trop petites et il fallait toujours vérifier qu'on avait bien le bon nombre de classes.

 - C'est quoi ces histoires d'affinités dans les préférences des profs ?
 Au début, j'avais codé un algo de Recuit Simulé. Il essayait de contenter tout le monde au mieux, avec des affinités et des rejets.
 Sur le papier, c'était pas mal, dans la pratique, beaucoup moins. Suite à un changement profond du code, j'ai enlevé cette partie.
 Je ne désespère pas de trouver un meilleur algorithme et du temps pour le coder...
 Pour l'instant donc, les affinités ne servent qu'à pré-affecter les classes qui ne présentent aucun conflit.

 - On peut aider ou proposer des trucs ?
 Avec plaisir ! N'hésitez pas à m'écrire. Par contre, je ne garantie pas d'intégrer vos changements ou vos demandes rapidement !


